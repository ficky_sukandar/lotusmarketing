﻿import { AutoCompleteService } from 'ionic2-auto-complete';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class DataProvider {

    data: Array<any> = [];
    constructor(public http: Http) { }

    getDataBarangJadi() {
        return new Promise(resolve =>
            this.http.get('assets/data/BarangJadi.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data;
                    console.log(this.data);
                    resolve(this.data);
                }));
    }

    getDataCapBarang() {
        return new Promise(resolve =>
            this.http.get('assets/data/CapBarang.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                }));
    }

    getDataCustomer() {
        return new Promise(resolve =>
            this.http.get('assets/data/Customer.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                }));
    }

    getDataDivisi() {
        return new Promise(resolve =>
            this.http.get('assets/data/Divisi.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                }));
    }

    getDataKadar() {
        return new Promise(resolve =>
            this.http.get('assets/data/Kadar.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                }));
    }

    getDataWarnaBJ() {
        return new Promise(resolve =>
            this.http.get('assets/data/WarnaBJ.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data.sort(this.compareWarnaBJ);
                    resolve(this.data);
                }));
    }

    getDataBatu() {
        return new Promise(resolve =>
            this.http.get('assets/data/Batu.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                }));
    }

    getDataKategoriPerubahanModel() {
        return new Promise(resolve =>
            this.http.get('assets/data/KategoriPerubahanModel.json')
                .map(res => res.json())
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                }));
    }
    
    compareWarnaBJ(a, b) {
        if (a.NamaWarna < b.NamaWarna)
            return -1;
        if (a.NamaWarna > b.NamaWarna)
            return 1;
        return 0;
    }
}

@Injectable()
export class AutoCompleteCustomDataService implements AutoCompleteService {
    labelAttribute = "IdBarangJadi";

    constructor(private http: Http) {

    }
    getResults(keyword: string, custom: {}) {
        if (keyword === undefined || keyword.length < 10)
            return {};
        if (custom !== undefined) {
            console.log("custom");
            return this.http.get('assets/data/BarangJadi.json')
                .map(
                result => {
                    var filtered = result.json()
                        .filter(item => item.IdBarangJadi.toLowerCase().includes(keyword.toLowerCase()) && item.IdKadar == custom['IdKadar'])

                    return filtered;
                });

        }
    }
}
@Injectable()
export class AutoCompleteDataService implements AutoCompleteService {
    labelAttribute = "IdBarangJadi";

    constructor(private http: Http) {

    }
    getResults(keyword: string, custom: {}) {
        if (keyword === undefined || keyword.length < 10)
            return {};
        if (custom !== undefined) {
            console.log("none custom");
            return this.http.get('assets/data/BarangJadi.json')
                .map(
                result => {
                    return result.json()
                        .filter(item => item.IdBarangJadi.toLowerCase().includes(keyword.toLowerCase()))
                });
        }
    }
}