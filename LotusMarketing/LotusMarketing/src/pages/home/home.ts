import { Component } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { Http, Response, Jsonp, URLSearchParams } from '@angular/http';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    quote = {
        quoteText: '',
        quoteAuthor: ''
    };
    constructor(public navCtrl: NavController,
        public platform: Platform,
        public http: Http,
        private _jsonp: Jsonp) {
    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getRandomQuote();
            });
            this.getRandomQuote();
        });
    }

    getRandomQuote() {
        let params = new URLSearchParams();
        params.set('jsonp', 'JSONP_CALLBACK');
        params.set('method', 'getQuote');
        params.set('format', 'jsonp');
        params.set('lang', 'en');
        this._jsonp.get('https://api.forismatic.com/api/1.0/format=jsonp&lang=en&jsonp=JSON_CALLBACK', { search: params })
            .map(res => res.json())
            .subscribe(
            (data) => {
                this.quote = {
                    quoteText: data.quoteText,
                    quoteAuthor: data.quoteAuthor || 'Unknown'
                };
            },
            (error) => {
                this.quote = {
                    quoteText: 'There are basically two types of people. People who accomplish things, and people who claim to have accomplished things. The first group is less crowded',
                    quoteAuthor: 'Mark Twain'
                };
            });
    }
}
