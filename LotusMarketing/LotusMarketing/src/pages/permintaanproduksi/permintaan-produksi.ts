import { Component, ViewChild } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DataProvider } from '../../providers/data';
import { AutoCompleteCustomDataService } from '../../providers/data';
import { AutoCompleteComponent } from 'ionic2-auto-complete';
import { EmailComposer } from '@ionic-native/email-composer';
import { Screenshot } from '@ionic-native/screenshot';
import * as moment from 'moment';

@Component({
    selector: 'page-permintaan-produksi',
    templateUrl: 'permintaan-produksi.html'
})
export class PermintaanProduksiPage {
    pp = 'pending';
    isApp;
    public people: Array<any> = [];
    public permintaanProduksi: Array<any> = [];
    public submittedPermintaanProduksi: Array<any> = [];
    public allPermintaanProduksi: Array<any> = [];
    constructor(public navCtrl: NavController,
        public alertController: AlertController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider,
        private storage: Storage,
        private emailComposer: EmailComposer,
        public toastCtrl: ToastController) {

    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getPermintaanProduksi();
            });
            this.getPermintaanProduksi();
            if (this.platform.is('core') || this.platform.is('mobileweb')) {
                this.isApp = false;
            } else {
                this.isApp = true;
            }
        });
    }

    getPermintaanProduksi() {
        this.permintaanProduksi = [];
        this.submittedPermintaanProduksi = [];
        this.storage.get('permintaanProduksi').then((val) => {
            if (val === null) {
                this.storage.set('permintaanProduksi',
                    []);
            }
            else {
                for (var current in val) {

                    val[current].totalWeight = 0;
                    for (var i = 0; i < val[current]["barang"].length; i++) {
                        if (val[current]["barang"][i].Calculation) {
                            val[current].totalWeight += +val[current]["barang"][i].Quantity;
                        }
                        else {
                            val[current].totalWeight += val[current]["barang"][i].Quantity * val[current]["barang"][i].BeratBJ;
                        }
                    }
                    
                    if (val[current].submitted) {

                        this.submittedPermintaanProduksi.push(val[current]);
                    }
                    else {
                        this.permintaanProduksi.push(val[current]);
                    }
                }
            }
        });
    }
    ppDetail(item) {
        this.navCtrl.push(PPDetail, { item: item });
    }
    addPp() {
        this.navCtrl.push(AddPP, { data: this.permintaanProduksi, storage: this.storage });
    }
    editPp(item, index) {
        this.navCtrl.push(EditPP, { item: item, index: index, data: this.permintaanProduksi, storage: this.storage });
    }
    deletePp(index) {
        let confirm = this.alertController.create({
            title: 'Konfirmasi Penghapusan',
            message: 'Yakin untuk menghapus ' + this.permintaanProduksi[index].pelanggan.NamaCustomer + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: () => {
                        this.permintaanProduksi.splice(index, 1);
                        this.storage.set('permintaanProduksi', this.permintaanProduksi);
                    }
                }
            ]
        });
        confirm.present();
    }
    submitFPp(item) {
        item.submitted = true;
        this.allPermintaanProduksi = [];
        for (var i = 0; i < this.permintaanProduksi.length; i++) {
            this.allPermintaanProduksi.push(this.permintaanProduksi[i]);
        }

        for (var i = 0; i < this.submittedPermintaanProduksi.length; i++) {
            this.allPermintaanProduksi.push(this.submittedPermintaanProduksi[i]);
        }

        this.storage.set('permintaanProduksi', this.allPermintaanProduksi).then((val) =>
            this.getPermintaanProduksi()
        );
    }
    cancelFPp(item) {
        item.submitted = false;
        this.allPermintaanProduksi = [];
        for (var i = 0; i < this.permintaanProduksi.length; i++) {
            this.allPermintaanProduksi.push(this.permintaanProduksi[i]);
        }

        for (var i = 0; i < this.submittedPermintaanProduksi.length; i++) {
            this.allPermintaanProduksi.push(this.submittedPermintaanProduksi[i]);
        }

        this.storage.set('permintaanProduksi', this.allPermintaanProduksi).then((val) =>
            this.getPermintaanProduksi()
        );
    }
    submitPp(item) {
        var stringify = JSON.stringify(item);
        var base64 = btoa(stringify.replace(/[\u00A0-\u2666]/g, function (c) {
            return '&#' + c.charCodeAt(0) + ';';
        }));
        this.emailComposer.addAlias('gmail', 'com.google.android.gm');
        let email = {
            app: 'gmail',
            to: 'sales@llpgold.com',
            attachments: ['base64:PP.json//' + base64 + ''],
            subject: 'Permintaan Produksi Baru',
            body: 'Berikut dilampirkan file yang berisi informasi mengenai PP baru',
            isHtml: true
        };

        this.emailComposer.open(email).then((message) => {

            if (message + "" == "OK") {
                this.presentToast("Email terkirim");
                item.resubmitted = false;
                item.submitted = true;
                this.allPermintaanProduksi = [];
                for (var i = 0; i < this.permintaanProduksi.length; i++) {
                    this.allPermintaanProduksi.push(this.permintaanProduksi[i]);
                }

                for (var i = 0; i < this.submittedPermintaanProduksi.length; i++) {
                    this.allPermintaanProduksi.push(this.submittedPermintaanProduksi[i]);
                }

                this.storage.set('permintaanProduksi', this.allPermintaanProduksi).then((val) =>
                    this.getPermintaanProduksi()
                );
            }
            else {
                this.presentToast("Email gagal terkirim");
            }
        });
    }

    reSubmitPp(item) {

        this.emailComposer.addAlias('gmail', 'com.google.android.gm');
        let email = {
            app: 'gmail',
            to: 'sales@llpgold.com',
            attachments: ['base64:PP.json//' + btoa(JSON.stringify(item)) + ''],
            subject: 'Kirim Ulang PP',
            body: 'Pengiriman ulang PP',
            isHtml: true
        };

        this.emailComposer.open(email).then((message) => {

            if (message + "" == "OK") {
                this.presentToast("Email terkirim");
            }
            else {
                this.presentToast("Email gagal terkirim");
            }
        });
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }
}

@Component({
    templateUrl: 'permintaan-produksi-detail.html',
})
export class PPDetail {
    item;
    imageUrlPath = 'http://202.150.154.158:21000/';
    content = '';
    constructor(params: NavParams,
        private screenshot: Screenshot,
        private emailComposer: EmailComposer,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        private storage: Storage,
        public platform: Platform) {
        this.item = params.data.item;
    }
    doCreateImageAndEmail() {

        this.platform.ready().then(() => {
            this.storage.get('settings').then((val) => {
                if (val === null) {
                    this.storage.set('settings',
                        []);
                }
                else {
                    this.imageUrlPath = val.imageUrlPath;
                }
            });
            this.content += '<div>Customer:' + this.item.pelanggan.NamaCustomer + '</div><br/>';
            this.content += '<div>Tanggal Order:' + moment(this.item.orderDate).format("DD MMMM YYYY") + '</div>';
            this.content += '<div>Keterangan:' + this.item.description + '</div>';
            this.content += '<div>Berat Total:' + this.item.totalWeight + ' gram</div>';
            for (var i = 0; i < Object.keys(this.item.barang).length; i++) {
                if (i == 0) {
                    this.content += '<div>Detail Barang (' + Object.keys(this.item.barang).length + ')</div>';
                }
                this.content += '<div>' + this.item.barang[i].IdBarangJadi + '</div>';
                this.content += '<div><img src="' + this.imageUrlPath + this.item.barang[i].IdBarangJadi + '.jpg" /></div>';
                this.content += '<div>Jumlah: ' + this.item.barang[i].Quantity + ' ' + (this.item.barang[i].Calculation) ? " Pieces" : " Gram" + '</div>';

            }
            this.emailComposer.addAlias('gmail', 'com.google.android.gm');
            let email = {
                app: 'gmail',
                to: 'sales@llpgold.com',
                subject: 'Order' + moment(this.item.orderDate).format("DD MMMM YYYY"),
                body: this.content,
                isHtml: true
            };

            this.emailComposer.open(email).then((message) => {

                if (message + "" == "OK") {
                    this.presentToast("Email terkirim");
                }
                else {
                    this.presentToast("Email gagal terkirim");
                }
            });
        });
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }
}

@Component({
    templateUrl: 'tambah-permintaan-produksi.html',
})
export class AddPP {

    item;
    permintaanProduksi: Array<any> = [];
    storage;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public alertCtrl: AlertController) {
        this.item = {};
        this.item.pelanggan = {};
        this.storage = params.data.storage;
        this.permintaanProduksi = params.data.data;
        this.item.orderDate = new Date().toISOString();
        var tempDate = new Date();
        tempDate.setDate(tempDate.getDate() + 14);
        this.item.deadline = tempDate.toISOString();
        this.item.barang = [];
    }
    loadCustomer() {
        this.navCtrl.push(CustomerChooser, {
            data: this.item.pelanggan,
            callback: this.myCallbackFunction
        });
    }
    orderDateChanged() {
        var tempDate = new Date(this.item.orderDate);
        tempDate.setDate(tempDate.getDate() + 14);
        this.item.deadline = tempDate.toISOString();
    }
    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.pelanggan = _params;
            resolve();
        });
    }
    doAdd(item) {
        item.submitted = false;
        this.permintaanProduksi.push(item);
        this.storage.set('permintaanProduksi', this.permintaanProduksi);
        this.navCtrl.pop();
    }
    openEditItem(item, index) {
        this.navCtrl.push(EditBarangDetail, { currentBarangCollection: this.item.barang, currentBarang: item, currentBarangIndex: index });
    }
    addDetailBarang() {
        this.navCtrl.push(AddBarangDetail, { currentItem: this.item });
    }
    doUpdateItem(item, index) {
        this.item.barang[index] = item;
        this.storage.set('permintaanProduksi', this.permintaanProduksi);
    }
    deleteItem(index) {
        let confirm = this.alertCtrl.create({
            title: 'Konfirmasi Penghapusan',
            message: 'Yakin untuk menghapus ' + this.item.barang[index].IdBarangJadi + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: () => {
                        this.item.barang.splice(index, 1);
                        this.storage.set('permintaanProduksi', this.permintaanProduksi);
                    }
                }
            ]
        });
        confirm.present();
    }
}

@Component({
    templateUrl: 'ubah-permintaan-produksi.html',
})
export class EditPP {
    item;
    index;
    permintaanProduksi: Array<any> = [];
    storage;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public alertCtrl: AlertController) {
        this.item = params.data.item;
        this.item.barang = params.data.item.barang || [];
        this.index = params.data.index;
        this.permintaanProduksi = params.data.data;
        this.storage = params.data.storage;

        console.log(this.item);
    }

    orderDateChanged() {
        var tempDate = new Date(this.item.orderDate);
        tempDate.setDate(tempDate.getDate() + 14);
        this.item.deadline = tempDate.toISOString();
    }
    loadCustomer() {
        this.navCtrl.push(CustomerChooser, {
            data: this.item.pelanggan,
            callback: this.myCallbackFunction
        });
    }
    doUpdate(item) {
        this.permintaanProduksi[this.index] = item;
        this.permintaanProduksi[this.index].submitted = false;
        this.storage.set('permintaanProduksi', this.permintaanProduksi);
        this.navCtrl.pop();
    }
    addDetailBarang() {
        this.navCtrl.push(AddBarangDetail, { currentItem: this.permintaanProduksi[this.index] });
    }
    openEditItem(item, index) {
        this.navCtrl.push(EditBarangDetail, { currentItem: this.permintaanProduksi[this.index], currentBarang: item, currentBarangIndex: index });
    }
    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.pelanggan = _params;
            resolve();
        });
    }
    doUpdateItem(item, index) {

        this.permintaanProduksi[this.index].barang[index] = item;
        this.storage.set('permintaanProduksi', this.permintaanProduksi);
    }
    deleteItem(index) {
        let confirm = this.alertCtrl.create({
            title: 'Konfirmasi Penghapusan',
            message: 'Yakin untuk menghapus ' + this.item.barang[index].IdBarangJadi + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: () => {
                        this.item.barang.splice(index, 1);
                        this.storage.set('permintaanProduksi', this.permintaanProduksi);
                    }
                }
            ]
        });
        confirm.present();
    }
}

@Component({
    templateUrl: 'customer-chooser.html'
})
export class CustomerChooser {
    public people: Array<any> = [];
    selectedCustomerid;
    selectedCustomer;
    callback;
    pelanggan;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public alertController: AlertController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider) {
        this.callback = params.data.callback;
        this.pelanggan = params.data.data || [];

    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getCustomers();
            });
            this.getCustomers();
        });
    };
    getCustomers() {
        this.people = [];
        let loader = this.loadingCtrl.create({
            content: "Retrieving customers..."
        });
        loader.present();
        this.dataProvider.getDataCustomer().then(
            data => {
                loader.dismiss();
                if (data) {
                    for (var current in data) {
                        this.people.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving customer data: Data object is empty');
                }
            },
            error => {
                loader.dismiss();
                console.error('Error retrieving customer data');
                console.dir(error);
            }
        );
    };


    doSelectCustomer(event: any): void {
        for (var i = 0; i < this.people.length; i++) {
            if (this.people[i].IdCustomer == this.selectedCustomerid) {
                this.pelanggan = this.people[i];
                this.callback(this.pelanggan).then(() => {
                    this.navCtrl.pop();
                });
                break;
            }
        }
    };
    refreshPage() {
        this.getCustomers();
    };
}

@Component({
    templateUrl: 'permintaan-produksi-tambah-detail.html'
})
export class AddBarangDetail {
    currentItem;
    item;
    permintaanProduksi: Array<any> = [];
    warnaBJCollection: Array<any> = [];
    capCollection: Array<any> = [];
    kadarCollection: Array<any> = [];
    batuCollection: Array<any> = [];
    searchbarSelected = {
        IdBarangJadi: '',
        BeratBJ: '',
        IdGrupWarna: '',
        IdWarnaBJ: '',
        IdCap: '',
        IdDivisi: '',
        IdKadar: '',
        GrupWarna: '',
        Divisi: '',
        Kadar: ''
    };
    selectedCap;
    selectedWarnaBJ;
    index;
    imageUrlPath = 'http://202.150.154.158:21000/';
    @ViewChild('searchbar') searchbar: AutoCompleteComponent;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public autoCompleteDataService: AutoCompleteCustomDataService,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider,
        private storage: Storage,
    ) {
        this.currentItem = params.data.currentItem;

        this.item = {
            Quantity: 1,
            IdBarangJadi: '',
            BeratBJ: '',
            IdGrupWarna: '',
            IdWarnaBJ: '',
            IdCap: '',
            IdDivisi: '',
            IdKadar: '',
            IdBatu: '',
            GrupWarna: '',
            Divisi: '',
            Kadar: '',
            Calculation: false
        };
    }
    kadarChanged() {
        if (this.item.IdKadar != '') {
            this.item.Kadar = this.kadarCollection
                .filter(item => item.IdKadar == this.item.IdKadar)[0]["Kadar"];
            var checkCap = this.capCollection.filter(item => item.IdKadar == this.item.IdKadar);
            if (checkCap.length == 1) {
                this.item.IdCap = checkCap[0].IdCap;
                this.item.Cap = checkCap[0].NamaCap;
            }
        }
    };
    capChanged() {
        if (this.item.IdCap != '') {
            this.item.Cap = this.capCollection
                .filter(item => item.IdCap == this.item.IdCap)[0]["NamaCap"];
        }
    };
    warnaBJChanged() {
        if (this.item.IdWarnaBJ != '') {
            this.item.WarnaBJ = this.warnaBJCollection
                .filter(item => item.IdWarnaBJ == this.item.IdWarnaBJ)[0]["NamaWarna"];
        }
    };
    batuChanged() {
        if (this.item.IdBatu != '') {
            this.item.Batu = this.batuCollection
                .filter(item => item.IdBatu == this.item.IdBatu)[0]["NamaBatu"];
        }
    };
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getKadarAndCaps();
            });
            this.getKadarAndCaps();
        });
    };
    getKadarAndCaps() {
        this.storage.get('settings').then((val) => {
            if (val === null) {
                this.storage.set('settings',
                    []);
            }
            else {
                this.imageUrlPath = val.imageUrlPath;
            }
        });
        this.warnaBJCollection = [];
        this.capCollection = [];
        this.kadarCollection = [];
        this.dataProvider.getDataKadar().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.kadarCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving kadar data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving kadar data');
                console.dir(error);
            }
        );
        this.dataProvider.getDataCapBarang().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.capCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving cap data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving data data');
                console.dir(error);
            }
        );

        this.dataProvider.getDataWarnaBJ().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.warnaBJCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving warnaBJ data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving data data');
                console.dir(error);
            }
        );

        this.dataProvider.getDataBatu().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.batuCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving batu data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving data data');
                console.dir(error);
            }
        );
    };
    itemSelected(data) {
        if (data == null)
            return;
        this.item.IdBarangJadi = data.IdBarangJadi;
        this.item.BeratBJ = data.BeratBJ;
        this.item.IdDivisi = data.IdDivisi;
        this.item.IdGrupWarna = data.IdGrupWarna;
        this.item.Divisi = data.Divisi;
        this.item.GrupWarna = data.GrupWarna;
        this.item.Kadar = data.Kadar;
    }

    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.barang = _params;
            resolve();
        });
    }

    doAddOnly(item) {
        this.currentItem.barang.push(item);
        this.presentToast("Barang " + item.IdBarangJadi + " berhasil ditambahkan");
        var selectedIdKadar = this.item.IdKadar;
        var selectedIdCap = this.item.IdCap;
        var selectedIdWarnaBJ = this.item.IdWarnaBJ;
        var selectedIdBatu = this.item.IdBatu;
        var description = this.item.description;
        this.item = {
            Quantity: 1,
            IdBarangJadi: '',
            IdKadar: selectedIdKadar,
            IdCap: selectedIdCap,
            IdWarnaBJ: selectedIdWarnaBJ,
            IdBatu: selectedIdBatu,
            Calculation: false,
            Description: description
        };
        this.item.Kadar = this.kadarCollection
            .filter(item => item.IdKadar == this.item.IdKadar)[0]["Kadar"];
        this.item.Cap = this.capCollection
            .filter(item => item.IdCap == this.item.IdCap)[0]["NamaCap"];
        this.item.WarnaBJ = this.warnaBJCollection
            .filter(item => item.IdWarnaBJ == this.item.IdWarnaBJ)[0]["NamaWarna"];
        this.item.Batu = this.batuCollection
            .filter(item => item.IdBatu == this.item.IdBatu)[0]["NamaBatu"];
        this.searchbar.clearValue();
    }

    doAdd(item) {
        this.currentItem.barang.push(item);
        this.navCtrl.pop();
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }
}
@Component({
    templateUrl: 'permintaan-produksi-ubah-detail.html'
})
export class EditBarangDetail {
    currentBarangCollection: Array<any> = [];
    currentItem;
    currentBarangIndex;
    item;
    permintaanProduksi: Array<any> = [];
    warnaBJCollection: Array<any> = [];
    capCollection: Array<any> = [];
    kadarCollection: Array<any> = [];
    batuCollection: Array<any> = [];
    searchbarSelected = {
        IdBarangJadi: '',
        BeratBJ: '',
        IdGrupWarna: '',
        IdWarnaBJ: '',
        IdCap: '',
        IdDivisi: '',
        IdKadar: '',
        IdBatu: '',
        GrupWarna: '',
        Divisi: '',
        Kadar: ''
    };
    selectedKadar;
    selectedCap;
    selectedWarnaBJ;
    index;
    imageUrlPath = 'http://202.150.154.158:21000/';
    @ViewChild('searchbar') searchbar: AutoCompleteComponent;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public autoCompleteDataService: AutoCompleteCustomDataService,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider,
        public storage: Storage
    ) {
        this.index = params.data.index;
        this.currentItem = params.data.currentItem;
        this.currentBarangCollection = params.data.currentBarangCollection;
        this.item = params.data.currentBarang;
        this.currentBarangIndex = params.data.currentBarangIndex;
    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getKadarAndCaps();
            });
            this.getKadarAndCaps();
        });
    };
    kadarChanged() {
        if (this.item.IdKadar != '') {
            this.item.Kadar = this.kadarCollection
                .filter(item => item.IdKadar == this.item.IdKadar)[0]["Kadar"];
        }
        var checkCap = this.capCollection.filter(item => item.IdKadar == this.item.IdKadar);
        if (checkCap.length == 1) {
            this.item.IdCap = checkCap[0].IdCap;
            this.item.Cap = checkCap[0].NamaCap;
        }
    };
    capChanged() {
        if (this.item.IdCap != '') {
            this.item.Cap = this.capCollection
                .filter(item => item.IdCap == this.item.IdCap)[0]["NamaCap"];
        }
    };
    warnaBJChanged() {
        if (this.item.IdWarnaBJ != '') {
            this.item.WarnaBJ = this.warnaBJCollection
                .filter(item => item.IdWarnaBJ == this.item.IdWarnaBJ)[0]["NamaWarna"];
        }
    };
    batuChanged() {
        if (this.item.IdBatu != '') {
            this.item.Batu = this.batuCollection
                .filter(item => item.IdBatu == this.item.IdBatu)[0]["NamaBatu"];
        }
    };
    getKadarAndCaps() {
        this.storage.get('settings').then((val) => {
            if (val === null) {
                this.storage.set('settings',
                    []);
            }
            else {
                this.imageUrlPath = val.imageUrlPath;
            }
        });
        this.warnaBJCollection = [];
        this.capCollection = [];
        this.kadarCollection = [];
        this.dataProvider.getDataKadar().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.kadarCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving kadar data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving kadar data');
                console.dir(error);
            }
        );
        this.dataProvider.getDataCapBarang().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.capCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving cap data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving data data');
                console.dir(error);
            }
        );

        this.dataProvider.getDataWarnaBJ().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.warnaBJCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving warnaBJ data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving data data');
                console.dir(error);
            }
        );
        this.dataProvider.getDataBatu().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.batuCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving batu data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving data data');
                console.dir(error);
            }
        );
    };
    itemSelected(data) {
        if (data == null)
            return;
        this.item.IdBarangJadi = data.IdBarangJadi;
        this.item.BeratBJ = data.BeratBJ;
        this.item.IdDivisi = data.IdDivisi;
        this.item.IdGrupWarna = data.IdGrupWarna;
        this.item.Divisi = data.Divisi;
        this.item.GrupWarna = data.GrupWarna;
        this.item.Kadar = data.Kadar;
    }

    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.barang = _params;
            resolve();
        });
    }

    doEdit(item) {
        if (this.currentItem !== undefined) {
            console.log("edit mode");
            this.currentItem.barang[this.currentBarangIndex] = item;
        }
        else {
            console.log("add mode");
            this.currentBarangCollection[this.currentBarangIndex] = item;
        }
        this.navCtrl.pop();
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }
}
