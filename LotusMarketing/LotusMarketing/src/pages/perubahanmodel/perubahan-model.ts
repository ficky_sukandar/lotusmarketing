import { Component, ViewChild } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DataProvider } from '../../providers/data';
import { AutoCompleteDataService } from '../../providers/data';
import { AutoCompleteComponent } from 'ionic2-auto-complete';
import { EmailComposer } from '@ionic-native/email-composer';
import { Screenshot } from '@ionic-native/screenshot';
import * as moment from 'moment';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';

@Component({
    selector: 'page-perubahan-model',
    templateUrl: 'perubahan-model.html'
})
export class PerubahanModelPage {
    pm = 'pending';
    isApp;
    public people: Array<any> = [];
    public perubahanModel: Array<any> = [];
    public submittedPerubahanModel: Array<any> = [];
    public allPerubahanModel: Array<any> = [];
    public divisiCollection: Array<any> = [];
    constructor(public navCtrl: NavController,
        public alertController: AlertController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider,
        private storage: Storage,
        private emailComposer: EmailComposer,
        public toastCtrl: ToastController) {

    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getPerubahanModel();
            });
            this.getPerubahanModel();
            if (this.platform.is('core') || this.platform.is('mobileweb')) {
                this.isApp = false;
            } else {
                this.isApp = true;
            }
        });
    }

    getPerubahanModel() {
        this.perubahanModel = [];
        this.submittedPerubahanModel = [];
        let loader = this.loadingCtrl.create({
            content: "Mengambil data PO..."
        });
        loader.present();

        this.divisiCollection = [];
        this.dataProvider.getDataDivisi().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.divisiCollection.push(data[current]);
                    }
                    console.log(this.divisiCollection);
                    this.storage.get('perubahanModel').then((val) => {
                        if (val === null) {
                            this.storage.set('perubahanModel',
                                []);
                        }
                        else {
                            for (var current in val) {

                                val[current].totalWeight = 0;
                                var currentDivisi = this.divisiCollection.filter(item => item.Id == val[current].idDivisi)[0];
                                if (currentDivisi !== undefined)
                                    val[current].divisionName = currentDivisi.NamaDivisi;
                                for (var i = 0; i < val[current]["barang"].length; i++) {
                                    if (val[current]["barang"][i].Calculation) {
                                        val[current].totalWeight += +val[current]["barang"][i].Quantity;
                                    }
                                    else {
                                        val[current].totalWeight += val[current]["barang"][i].Quantity * val[current]["barang"][i].BeratBJ;
                                    }
                                }
                                if (val[current].submitted) {

                                    this.submittedPerubahanModel.push(val[current]);
                                }
                                else {
                                    this.perubahanModel.push(val[current]);
                                }
                            }
                            console.log(val);
                        }
                    });
                } else {
                    console.error('Error retrieving kadar data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving kadar data');
                console.dir(error);
            }
        );


        loader.dismiss();
    }
    pmDetail(item) {
        this.navCtrl.push(PMDetail, { item: item });
    }
    addPm() {
        this.navCtrl.push(AddPM, { data: this.perubahanModel, storage: this.storage });
    }
    editPm(item, index) {
        this.navCtrl.push(EditPM, { item: item, index: index, data: this.perubahanModel, storage: this.storage });
    }
    deletePm(index) {
        let confirm = this.alertController.create({
            title: 'Konfirmasi Penghapusan',
            message: 'Yakin untuk menghapus ' + this.perubahanModel[index].pelanggan.NamaCustomer + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: () => {
                        this.perubahanModel.splice(index, 1);
                        this.storage.set('perubahanModel', this.perubahanModel);
                    }
                }
            ]
        });
        confirm.present();
    }
    submitFPm(item) {
        item.submitted = true;

        this.allPerubahanModel = [];
        for (var i = 0; i < this.perubahanModel.length; i++) {
            this.allPerubahanModel.push(this.perubahanModel[i]);
        }

        for (var i = 0; i < this.submittedPerubahanModel.length; i++) {
            this.allPerubahanModel.push(this.submittedPerubahanModel[i]);
        }

        this.storage.set('perubahanModel', this.allPerubahanModel).then((val) =>
            this.getPerubahanModel()
        );
    }
    cancelFPm(item) {
        item.submitted = false;

        this.allPerubahanModel = [];
        for (var i = 0; i < this.perubahanModel.length; i++) {
            this.allPerubahanModel.push(this.perubahanModel[i]);
        }

        for (var i = 0; i < this.submittedPerubahanModel.length; i++) {
            this.allPerubahanModel.push(this.submittedPerubahanModel[i]);
        }

        this.storage.set('perubahanModel', this.allPerubahanModel).then((val) =>
            this.getPerubahanModel()
        );
    }
    submitPm(item) {

        this.emailComposer.addAlias('gmail', 'com.google.android.gm');
        for (var i = 0; i < item.images.length; i++) {
            item.base64Image = this.getBase64Image(item.images[i]);
        }
        let email = {
            apm: 'gmail',
            to: 'sales@llpgold.com',
            attachments: ['base64:PM.json//' + btoa(JSON.stringify(item)) + ''],
            subject: 'New PM',
            body: 'Here is new PM to be proccessed',
            isHtml: true
        };

        this.emailComposer.open(email).then((message) => {

            if (message + "" == "OK") {
                this.presentToast("Email terkirim");
                item.resubmitted = false;
                item.submitted = true;

                this.allPerubahanModel = [];
                for (var i = 0; i < this.perubahanModel.length; i++) {
                    this.allPerubahanModel.push(this.perubahanModel[i]);
                }

                for (var i = 0; i < this.submittedPerubahanModel.length; i++) {
                    this.allPerubahanModel.push(this.submittedPerubahanModel[i]);
                }

                this.storage.set('perubahanModel', this.allPerubahanModel).then((val) =>
                    this.getPerubahanModel()
                );
            }
            else {
                this.presentToast("Email gagal terkirim");
            }
        });
    }
    getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }

    reSubmitPm(item) {

        this.emailComposer.addAlias('gmail', 'com.google.android.gm');
        let email = {
            apm: 'gmail',
            to: 'sales@llpgold.com',
            attachments: ['base64:PM.json//' + btoa(JSON.stringify(item)) + ''],
            subject: 'Kirim Ulang PM',
            body: 'Pengiriman ulang PM',
            isHtml: true
        };

        this.emailComposer.open(email).then((message) => {

            if (message + "" == "OK") {
                this.presentToast("Email terkirim");
                item.resubmitted = true;
                item.submitted = true;

                this.allPerubahanModel = [];
                for (var i = 0; i < this.perubahanModel.length; i++) {
                    this.allPerubahanModel.push(this.perubahanModel[i]);
                }

                for (var i = 0; i < this.submittedPerubahanModel.length; i++) {
                    this.allPerubahanModel.push(this.submittedPerubahanModel[i]);
                }

                this.storage.set('perubahanModel', this.allPerubahanModel).then((val) =>
                    this.getPerubahanModel()
                );
            }
            else {
                this.presentToast("Email gagal terkirim");
            }
        });
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }
}

@Component({
    templateUrl: 'perubahan-model-detail.html',
})
export class PMDetail {
    item;
    imageUrlPath = 'http://202.150.154.158:21000/';
    content = '';
    constructor(params: NavParams,
        private screenshot: Screenshot,
        private emailComposer: EmailComposer,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public platform: Platform) {
        this.item = params.data.item;
        console.log(this.item);
        console.log(this.item.barang);
    }
    doCreateImageAndEmail() {
        this.platform.ready().then(() => {
            this.content += '<div>Customer:' + this.item.pelanggan.NamaCustomer + '</div><br/>';
            this.content += '<div>Tanggal Order:' + moment(this.item.orderDate).format("DD MMMM YYYY") + '</div>';
            this.content += '<div>Keterangan:' + this.item.description + '</div>';
            this.content += '<div>Berat Total:' + this.item.totalWeight + ' gram</div>';
            for (var i = 0; i < Object.keys(this.item.barang).length; i++) {
                if (i == 0) {
                    this.content += '<div>Detail Barang (' + Object.keys(this.item.barang).length + ')</div>';
                }
                this.content += '<div>' + this.item.barang[i].IdBarangJadi + '</div>';
                this.content += '<div><img src="' + this.imageUrlPath + this.item.barang[i].IdBarangJadi + '.jpg" /></div>';
                this.content += '<div>Jumlah: ' + this.item.barang[i].Quantity + ' ' + (this.item.barang[i].Calculation) ? " Pieces" : " Gram" + '</div>';

            }
            this.emailComposer.addAlias('gmail', 'com.google.android.gm');
            let email = {
                apm: 'gmail',
                to: 'sales@llpgold.com',
                subject: 'Order' + moment(this.item.orderDate).format("DD MMMM YYYY"),
                body: this.content,
                isHtml: true
            };

            this.emailComposer.open(email).then((message) => {

                if (message + "" == "OK") {
                    this.presentToast("Email terkirim");
                }
                else {
                    this.presentToast("Email gagal terkirim");
                }
            });
        });
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }
}

@Component({
    templateUrl: 'tambah-perubahan-model.html',
})
export class AddPM {

    item;
    perubahanModel: Array<any> = [];
    divisiCollection: Array<any> = [];
    kategoriPerubahanModelCollection: Array<any> = [];
    storage;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public dataProvider: DataProvider,
        public platform: Platform) {
        this.item = {};
        this.item.pelanggan = {};
        this.storage = params.data.storage;
        this.perubahanModel = params.data.data;
        this.item.date = new Date().toISOString();
        this.item.barang = [];
    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getDivisions();
            });
            this.getDivisions();
        });
    };

    getDivisions() {
        this.divisiCollection = [];
        this.dataProvider.getDataDivisi().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.divisiCollection.push(data[current]);
                    }
                    console.log(this.divisiCollection);
                } else {
                    console.error('Error retrieving kadar data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving kadar data');
                console.dir(error);
            }
        );
    };

    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.pelanggan = _params;
            resolve();
        });
    }
    doAdd(item) {
        item.submitted = false;
        this.perubahanModel.push(item);
        this.storage.set('perubahanModel', this.perubahanModel);
        this.navCtrl.pop();
    }

    loadCustomer() {
        this.navCtrl.push(PMCustomerChooser, {
            data: this.item.pelanggan,
            callback: this.myCallbackFunction
        });
    }
    openEditItem(item, index) {
        this.navCtrl.push(EditPMBarangDetail, { currentBarangCollection: this.item.barang, currentBarang: item, currentBarangIndex: index });
    }
    addDetailBarang() {
        this.navCtrl.push(AddPMBarangDetail, { currentItem: this.item });
    }
    doUpdateItem(item, index) {

        this.item.barang[index] = item;
        this.storage.set('perubahanModel', this.perubahanModel);
    }
    deleteItem(index) {
        let confirm = this.alertCtrl.create({
            title: 'Konfirmasi Penghapusan',
            message: 'Yakin untuk menghapus ' + this.item.barang[index].IdBarangJadi + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: () => {
                        this.item.barang.splice(index, 1);
                        this.storage.set('perubahanModel', this.perubahanModel);
                    }
                }
            ]
        });
        confirm.present();
    }
}

@Component({
    templateUrl: 'ubah-perubahan-model.html',
})
export class EditPM {
    item;
    index;
    perubahanModel: Array<any> = [];
    divisiCollection: Array<any> = [];
    storage;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public dataProvider: DataProvider,
        private platform: Platform) {
        this.item = params.data.item;
        this.item.barang = params.data.item.barang || [];
        this.index = params.data.index;
        this.perubahanModel = params.data.data;
        this.storage = params.data.storage;

    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getDivisions();
            });
            this.getDivisions();
        });
    };

    getDivisions() {
        this.divisiCollection = [];
        this.dataProvider.getDataDivisi().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.divisiCollection.push(data[current]);
                    }
                    console.log(this.divisiCollection);
                } else {
                    console.error('Error retrieving kadar data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving kadar data');
                console.dir(error);
            }
        );
    };
    loadCustomer() {
        this.navCtrl.push(PMCustomerChooser, {
            data: this.item.pelanggan,
            callback: this.myCallbackFunction
        });
    }
    doUpdate(item) {
        this.perubahanModel[this.index] = item;
        this.perubahanModel[this.index].submitted = false;
        this.storage.set('perubahanModel', this.perubahanModel);
        this.navCtrl.pop();
    }
    addDetailBarang() {
        this.navCtrl.push(AddPMBarangDetail, { currentItem: this.perubahanModel[this.index] });
    }
    openEditItem(item, index) {
        this.navCtrl.push(EditPMBarangDetail, { currentItem: this.perubahanModel[this.index], currentBarang: item, currentBarangIndex: index });
    }
    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.pelanggan = _params;
            resolve();
        });
    }
    doUpdateItem(item, index) {

        this.perubahanModel[this.index].barang[index] = item;
        this.storage.set('perubahanModel', this.perubahanModel);
    }
    deleteItem(index) {
        let confirm = this.alertCtrl.create({
            title: 'Konfirmasi Penghapusan',
            message: 'Yakin untuk menghapus ' + this.item.barang[index].IdBarangJadi + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: () => {
                        this.item.barang.splice(index, 1);
                        this.storage.set('perubahanModel', this.perubahanModel);
                    }
                }
            ]
        });
        confirm.present();
    }
}

@Component({
    templateUrl: 'customer-chooser.html'
})
export class PMCustomerChooser {
    public people: Array<any> = [];
    selectedCustomerid;
    selectedCustomer;
    callback;
    pelanggan;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public alertController: AlertController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider) {
        this.callback = params.data.callback;
        this.pelanggan = params.data.data || [];

    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getCustomers();
            });
            this.getCustomers();
        });
    };
    getCustomers() {
        this.people = [];
        let loader = this.loadingCtrl.create({
            content: "Retrieving customers..."
        });
        loader.present();
        this.dataProvider.getDataCustomer().then(
            data => {
                loader.dismiss();
                if (data) {
                    for (var current in data) {
                        this.people.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving customer data: Data object is empty');
                }
            },
            error => {
                loader.dismiss();
                console.error('Error retrieving customer data');
                console.dir(error);
            }
        );
    };
    doSelectCustomer(event: any): void {
        for (var i = 0; i < this.people.length; i++) {
            if (this.people[i].IdCustomer == this.selectedCustomerid) {
                this.pelanggan = this.people[i];
                this.callback(this.pelanggan).then(() => {
                    this.navCtrl.pop();
                });
                break;
            }
        }
    };
    refreshPage() {
        this.getCustomers();
    };
}

@Component({
    templateUrl: 'perubahan-model-tambah-detail.html'
})
export class AddPMBarangDetail {
    currentItem;
    item;
    perubahanModel: Array<any> = [];
    kategoriPerubahanModelCollection: Array<any> = [];
    kadarCollection: Array<any> = [];
    imageGalleryCollection: Array<any> = [];
    imageBase64Collection: Array<any> = [];
    storage;
    searchbarSelected = {};
    index;
    imageUrlPath = 'http://202.150.154.158:21000/';
    imageCollection = '';
    @ViewChild('searchbar') searchbar: AutoCompleteComponent;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public autoCompleteDataService: AutoCompleteDataService,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider,
        private camera: Camera,
        private imagePicker: ImagePicker
    ) {
        this.storage = params.data.storage;
        this.perubahanModel = params.data.data;
        this.currentItem = params.data.currentItem;

        this.item = {
            Quantity: 1,
            IdBarangJadi: '',
            BeratBJ: '',
            IdKadar: '',
            Divisi: '',
            Kadar: '',
            Calculation: false
        };
    }

    options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getKadarAndKategoriPerubahanModels();
            });
            this.getKadarAndKategoriPerubahanModels();
        });
    };
    getKadarAndKategoriPerubahanModels() {
        this.kadarCollection = [];
        this.dataProvider.getDataKategoriPerubahanModel().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.kategoriPerubahanModelCollection.push(data[current]);
                    }
                    console.log(this.kategoriPerubahanModelCollection);
                } else {
                    console.error('Error retrieving kategori perubahan Model data: Data object is empty');
                }
            });
        this.dataProvider.getDataKadar().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.kadarCollection.push(data[current]);
                    }
                    console.log(this.kadarCollection);
                } else {
                    console.error('Error retrieving kadar data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving kadar data');
                console.dir(error);
            }
        );
    };
    doLoadImagePicker() {
        this.item.images = [];
        this.imagePicker.getPictures(this.options).then((results) => {
            for (var i = 0; i < results.length; i++) {
                this.item.imageCollection += results[i] + "\n";
                this.item.imageBase64Collection.push('data:image/jpeg;base64,' + results[i]);
                this.item.images.push(results[i]);
            }
            //let confirm = this.alertCtrl.create({
            //    title: 'Konfirmasi Penghapusan',
            //    message: this.imageCollection,
            //    buttons: [
            //        {
            //            text: 'Batal',
            //            handler: () => {
            //                console.log('Disagree clicked');
            //            }
            //        },
            //        {
            //            text: 'Ya',
            //            handler: () => {

            //            }
            //        }
            //    ]
            //});
            //confirm.present();
        }, (err) => { });
    }
    doCapture() {
        this.camera.getPicture(this.options).then((imageData) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            let confirm = this.alertCtrl.create({
                title: 'Konfirmasi Penghapusan',
                message: base64Image,
                buttons: [
                    {
                        text: 'Batal',
                        handler: () => {
                            console.log('Disagree clicked');
                        }
                    },
                    {
                        text: 'Ya',
                        handler: () => {

                        }
                    }
                ]
            });
            confirm.present();
        }, (err) => {
            // Handle error
        });
    };
    itemSelected(data) {
        if (data == null)
            return;
        console.log(data);
        this.item.IdBarangJadi = data.IdBarangJadi;
        this.item.BeratBJ = data.BeratBJ;
        this.item.IdDivisi = data.IdDivisi;
        this.item.IdGrupWarna = data.IdGrupWarna;
        this.item.IdKadar = data.IdKadar;
        this.item.Divisi = data.Divisi;
        this.item.GrupWarna = data.GrupWarna;
        this.item.Kadar = data.Kadar;
        this.item.Calculation = data.Calculation;
        console.log(this.item);
    }

    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.barang = _params;
            resolve();
        });
    }

    doAddOnly(item) {
        if (this.item.IdKategori == 3)
            item.IdBarangJadi = '';
        this.currentItem.barang.push(item);
        this.presentToast("Barang " + item.IdBarangJadi + " berhasil ditambahkan");
        var selectedIdKadar = this.item.IdKadar;
        this.item = {
            Quantity: 1,
            IdBarangJadi: '',
            IdKadar: selectedIdKadar,
            Calculation: false
        };
        this.searchbar.clearValue();
    }

    doAdd(item) {
        if (this.item.IdKategori == 3)
            item.IdBarangJadi = '';
        this.currentItem.barang.push(item);
        this.navCtrl.pop();
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }
}
@Component({
    templateUrl: 'perubahan-model-ubah-detail.html'
})
export class EditPMBarangDetail {
    currentBarangCollection: Array<any> = [];
    currentItem;
    currentBarangIndex;
    item;
    perubahanModel: Array<any> = [];
    kategoriPerubahanModelCollection: Array<any> = [];
    kadarCollection: Array<any> = [];
    imageGalleryCollection: Array<any> = [];
    imageBase64Collection: Array<any> = [];
    storage;
    index;
    imageCollection = '';
    imageUrlPath = 'http://202.150.154.158:21000/';
    @ViewChild('searchbar') searchbar: AutoCompleteComponent;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public autoCompleteDataService: AutoCompleteDataService,
        public toastCtrl: ToastController,
        private camera: Camera,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public dataProvider: DataProvider,
        private imagePicker: ImagePicker
    ) {
        this.index = params.data.index;
        this.currentItem = params.data.currentItem;
        this.currentBarangCollection = params.data.currentBarangCollection;
        this.item = params.data.currentBarang;
        this.currentBarangIndex = params.data.currentBarangIndex;
    }

    options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getKadarAndKategoriPerubahanModels();

            });
            this.getKadarAndKategoriPerubahanModels();
        });
    };
    getKadarAndKategoriPerubahanModels() {
        this.kadarCollection = [];
        this.kategoriPerubahanModelCollection = [];
        this.dataProvider.getDataKategoriPerubahanModel().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.kategoriPerubahanModelCollection.push(data[current]);
                    }
                    console.log(this.kategoriPerubahanModelCollection);
                } else {
                    console.error('Error retrieving kategori perubahan Model data: Data object is empty');
                }
            });
        this.dataProvider.getDataKadar().then(
            data => {
                if (data) {
                    for (var current in data) {
                        this.kadarCollection.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving kadar data: Data object is empty');
                }
            },
            error => {
                console.error('Error retrieving kadar data');
                console.dir(error);
            }
        );
    };
    itemSelected(data) {
        if (data == null)
            return;
        console.log(data);
        this.item.IdBarangJadi = data.IdBarangJadi;
        this.item.BeratBJ = data.BeratBJ;
        this.item.IdKadar = data.IdKadar;
        this.item.Kadar = data.Kadar;
        this.item.Calculation = data.Calculation;
        console.log(this.item);
    }

    myCallbackFunction = (_params) => {
        return new Promise((resolve, reject) => {
            this.item.barang = _params;
            resolve();
        });
    }

    doEdit(item) {
        console.log(item);
        if (this.currentItem !== undefined) {
            console.log("edit mode");
            this.currentItem.barang[this.currentBarangIndex] = item;
        }
        else {
            console.log("add mode");
            this.currentBarangCollection[this.currentBarangIndex] = item;
        }
        this.navCtrl.pop();
    }

    doLoadImagePicker() {
        this.item.images = [];
        this.imagePicker.getPictures(this.options).then((results) => {
            for (var i = 0; i < results.length; i++) {
                this.item.imageCollection += results[i] + "\n";
                this.item.imageBase64Collection.push('data:image/jpeg;base64,' + results[i]);
                this.item.images.push(results[i]);
            }
            //let confirm = this.alertCtrl.create({
            //    title: 'Konfirmasi Penghapusan',
            //    message: this.imageCollection,
            //    buttons: [
            //        {
            //            text: 'Batal',
            //            handler: () => {
            //                console.log('Disagree clicked');
            //            }
            //        },
            //        {
            //            text: 'Ya',
            //            handler: () => {

            //            }
            //        }
            //    ]
            //});
            //confirm.present();
        }, (err) => { });
    }
    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }

}
