import { Component, ViewChild } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DataProvider } from '../../providers/data';
import * as moment from 'moment';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})
export class SettingsPage {
    onFirstLoad = true;
    isApp;
    item;
    settings;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public platform: Platform,
        public loadingCtrl: LoadingController,
        private storage: Storage,
        private fileChooser: FileChooser,
        private filePath: FilePath,
        private toastCtrl: ToastController,
        public alertCtrl: AlertController) {
        this.item = {};
    };
    ionViewDidLoad() {
        console.log(this.onFirstLoad);
        if (this.onFirstLoad == true) {
            this.platform.ready().then(() => {
                this.onFirstLoad = false;
                document.addEventListener('resume', () => {
                    this.getSettings();
                });
                this.getSettings();
                if (this.platform.is('core') || this.platform.is('mobileweb')) {
                    this.isApp = false;
                } else {
                    this.isApp = true;
                }
            });
        }
    }

    getSettings() {

        this.settings = {};

        let loader = this.loadingCtrl.create({
            content: "Mengambil data Settings..."
        });
        loader.present();

        this.storage.get('settings').then((val) => {
            console.log(val);
            if (val === null) {
                this.storage.set('settings',
                    []);
            }
            else {
                this.item = val;
                console.log(val);
            }
        });

        loader.dismiss();
    }

    updateSettings(item) {
        this.settings = item;
        this.storage.set('settings', this.settings);
        this.presentToast("Pengaturan berhasil disimpan")
    };
    resetSettings() {
        this.item = {
            imageUrlPath: 'http://202.150.154.158:21000/'
        }
        this.settings = this.item;
        this.storage.set('settings', this.settings);
        this.presentToast("Pengaturan berhasil direset")
    };

    chooseFile() {
        this.fileChooser.open()
            .then(uri => {
                this.filePath.resolveNativePath(uri)
                    .then(filePath =>
                        this.item.imageUrlPath = filePath)
                    .catch(err => console.log(err));
                }
            )
            .catch(e => console.log(e));
    };

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    };
}
