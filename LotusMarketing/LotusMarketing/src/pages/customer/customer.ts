import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, Platform } from 'ionic-angular';
import { DataProvider } from '../../providers/data';

@Component({
  selector: 'page-customer',
  templateUrl: 'customer.html'
})
export class CustomerPage {

    public people:Array<any> = [];
    constructor(public navCtrl: NavController,
        public alertController: AlertController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public customer: DataProvider) {

    }
    onLink(url: string) {
        window.open(url);
    }
    ionViewDidLoad() {
        //Once the main view loads
        //and after the platform is ready...
        this.platform.ready().then(() => {
            //Setup a resume event listener
            document.addEventListener('resume', () => {
                //Get the local weather when the app resumes
                this.getCustomers();
            });
            //Populate the form with the current location data
            this.getCustomers();
        });
    }
    getCustomers() {
        this.people = [];
        //Create the loading indicator
        let loader = this.loadingCtrl.create({
            content: "Retrieving customers..."
        });
        //Show the loading indicator
        loader.present();
        this.customer.getDataCustomer().then(
            data => {
                //Hide the loading indicator
                loader.dismiss();
                //Now, populate the array with data from the weather service
                if (data) {
                    //We have data, so lets do something with it
                    for (var current in data)
                    {
                        this.people.push(data[current]);
                    }
                } else {
                    //This really should never happen
                    console.error('Error retrieving customer data: Data object is empty');
                }
            },
            error => {
                //Hide the loading indicator
                loader.dismiss();
                console.error('Error retrieving customer data');
                console.dir(error);
            }
        );
    }
    refreshPage() {
        this.getCustomers();
    }
}
