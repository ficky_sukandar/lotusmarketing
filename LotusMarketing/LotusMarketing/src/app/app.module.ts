import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage  } from '@ionic/storage';

import { LotusMarketingApp } from './app.component';
import { SettingsPage } from '../pages/settings/settings';
import { CustomerPage } from '../pages/customer/customer';
import { PermintaanProduksiPage } from '../pages/permintaanproduksi/permintaan-produksi';
import { PPDetail } from '../pages/permintaanproduksi/permintaan-produksi';
import { AddPP } from '../pages/permintaanproduksi/permintaan-produksi';
import { EditPP } from '../pages/permintaanproduksi/permintaan-produksi';
import { CustomerChooser } from '../pages/permintaanproduksi/permintaan-produksi';
import { AddBarangDetail } from '../pages/permintaanproduksi/permintaan-produksi';
import { EditBarangDetail } from '../pages/permintaanproduksi/permintaan-produksi';

import { PerubahanModelPage } from '../pages/perubahanmodel/perubahan-model';
import { PMDetail } from '../pages/perubahanmodel/perubahan-model';
import { AddPM } from '../pages/perubahanmodel/perubahan-model';
import { EditPM } from '../pages/perubahanmodel/perubahan-model';
import { PMCustomerChooser } from '../pages/perubahanmodel/perubahan-model';
import { AddPMBarangDetail } from '../pages/perubahanmodel/perubahan-model';
import { EditPMBarangDetail } from '../pages/perubahanmodel/perubahan-model';

import { HomePage } from '../pages/home/home';
//import { TabsPage } from '../pages/tabs/tabs';
import { DataProvider } from '../providers/data';
import { AutoCompleteCustomDataService } from '../providers/data';
import { AutoCompleteDataService } from '../providers/data';
import { ArrayFilterPipe, ArrayExactFilterPipe } from "../pipes/array-filter.pipe";
import { AutoCompleteModule, AutoCompleteComponent } from 'ionic2-auto-complete';
import { EmailComposer } from '@ionic-native/email-composer';
import { IonicImageLoader } from 'ionic-image-loader';
import { Screenshot } from '@ionic-native/screenshot';
import { JsonpModule } from '@angular/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

@NgModule({
    declarations: [
        LotusMarketingApp,
        HomePage,
        CustomerPage,
        SettingsPage,
        PermintaanProduksiPage,
        PPDetail,
        AddPP,
        EditPP,
        CustomerChooser,
        AddBarangDetail,
        EditBarangDetail,
        PerubahanModelPage,
        PMDetail,
        AddPM,
        EditPM,
        PMCustomerChooser,
        AddPMBarangDetail,
        EditPMBarangDetail,
        ArrayFilterPipe,
        ArrayExactFilterPipe
    ],
    imports: [
        JsonpModule,
        AutoCompleteModule,
        IonicModule.forRoot(LotusMarketingApp),
        IonicImageLoader.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        LotusMarketingApp,
        HomePage,
        CustomerPage,
        SettingsPage,
        PermintaanProduksiPage,
        PPDetail,
        AddPP,
        EditPP,
        CustomerChooser,
        AddBarangDetail,
        EditBarangDetail,
        PerubahanModelPage,
        PMDetail,
        AddPM,
        EditPM,
        PMCustomerChooser,
        AddPMBarangDetail,
        EditPMBarangDetail
    ],
    providers: [FilePath, FileChooser, ImagePicker, Camera, Screenshot, EmailComposer, AutoCompleteComponent,AutoCompleteCustomDataService, AutoCompleteDataService, DataProvider, Storage, { provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AppModule { }
