﻿import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "filter",
    pure: false
})
export class ArrayFilterPipe implements PipeTransform {

    transform(items: Array<any>, conditions: { [field: string]: any }): Array<any> {
        return items.filter(item => {
            for (let field in conditions) {
                if (conditions[field] === '' || conditions[field] === undefined) {
                    return true;
                }
                if (!item[field].includes(conditions[field].toUpperCase())) {
                    return false;
                }
            }
            return true;
        });
    }
}

@Pipe({
    name: "exactFilter",
    pure: false
})
export class ArrayExactFilterPipe implements PipeTransform {

    transform(items: Array<any>, conditions: { [field: string]: any }): Array<any> {
        return items.filter(item => {
            for (let field in conditions) {
                if (conditions[field] === '' || conditions[field] === undefined) {
                    return true;
                }
                if (item[field] != conditions[field]) {
                    return false;
                }
            }
            return true;
        });
    }
}
